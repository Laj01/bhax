    #include <iostream>

    class LZWTree
    {
    public:
        LZWTree () 
        {
            gyoker  = new Node();
            fa = gyoker;
        }

        ~LZWTree ()
        {
         szabadit (gyoker->egyesGyermek ());
         szabadit (gyoker->nullasGyermek ());
         delete gyoker;
     }

     void operator<<(char b)
     {
        if (b == '0')
        {

            if (!fa->nullasGyermek ())
            {
                Node *uj = new Node ('0');
                fa->ujNullasGyermek (uj);
                fa = gyoker;
            }
            else
            {
                fa = fa->nullasGyermek ();
            }
        }
        else
        {
            if (!fa->egyesGyermek ())
            {
                Node *uj = new Node ('1');
                fa->ujEgyesGyermek (uj);
                fa = gyoker;
            }
            else
            {
                fa = fa->egyesGyermek ();
            }
        }
    }


    LZWTree& operator= (LZWTree& copy) //másoló értékadás
    {
        szabadit(gyoker->egyesGyermek ()); //régi értéket törlöm
        szabadit(gyoker->nullasGyermek ());
        
        bejar(gyoker,copy.gyoker); //rekurzívan bejárom a fákat és átmásolom az értékeket

        fa = gyoker; //mindkét fában visszaugrok a gyökérhez 
        copy.fa = copy.gyoker;
    }

    void kiir (void)
    {
        melyseg = 0;
        kiir (gyoker);
    }
    void szabadit (void)
    {
        szabadit (gyoker->jobbEgy);
        szabadit (gyoker->balNulla);
    }

    private:

        class Node
        {
        public:
            Node (char b = '/'):betu (b), balNulla (0), jobbEgy (0) {};
            ~Node () {};
            Node *nullasGyermek () {
                return balNulla;
            }
            Node *egyesGyermek ()
            {
                return jobbEgy;
            }
            void ujNullasGyermek (Node * gy)
            {
                balNulla = gy;
            }
            void ujEgyesGyermek (Node * gy)
            {
                jobbEgy = gy;
            }
            
        private:
            friend class LZWTree;
            char betu;
            Node *balNulla;
            Node *jobbEgy;
            Node (const Node &);
            Node & operator=(const Node &);
        };

        Node *gyoker;
        Node *fa;
        int melyseg;

        LZWTree (const LZWTree &);
        LZWTree & operator=(const LZWTree &);



        void bejar (Node * masolat, Node * eredeti) //rekurzív famásolás, másoló értékadáshoz
        {

            if (eredeti != NULL) //ha létezik a másolandó fa
            {

                if ( !eredeti->nullasGyermek() ) //ha nem létezik az eredeti nullasgyermeke
                {
                    masolat->ujNullasGyermek(NULL);
                }
                else //ha létezik az eredeti nullásgyermeke
                {
                //létrehozni a masolat nullasgyermeket és meghívni újra a bejart
                Node* uj = new Node ('0');
                masolat->ujNullasGyermek (uj);
                bejar(masolat->nullasGyermek(), eredeti->nullasGyermek());
                }

                if ( !eredeti->egyesGyermek() ) //ha nem létezik az eredeti egyesgyermeke
                {
                    masolat->ujEgyesGyermek(NULL);
                }
                else //ha létezik az eredeti egyesgyermeke
                {
                //létrehozni a masolat egyesgyermeket és meghívni újra a bejart
                Node *uj = new Node ('1');
                    masolat->ujEgyesGyermek (uj);
                bejar(masolat->egyesGyermek(), eredeti->egyesGyermek());
                }
            }
            else //ha nem létezik a másolandó fa
            {
                masolat = NULL;
            }
        }

        void kiir (Node* elem)
        {
            if (elem != NULL)
            {
                ++melyseg;
                kiir (elem->jobbEgy);

                for (int i = 0; i < melyseg; ++i)
                    std::cout << "---";
                std::cout << elem->betu << "(" << melyseg - 1 << ")" << std::endl;
                kiir (elem->balNulla);
                --melyseg;
            }
        }
        void szabadit (Node * elem)
        {
            if (elem != NULL)
            {
                szabadit (elem->jobbEgy);
                szabadit (elem->balNulla);
                delete elem;
            }
        }

    };

    int
    main ()
    {
        char b;
        LZWTree binFa1, binFa2;

        while (std::cin >> b)
        {
            binFa1 << b;
        }

        binFa2 = binFa1;

        binFa2.kiir ();
        binFa1.kiir ();
        binFa2.szabadit ();
        binFa1.szabadit ();

        return 0;
    }