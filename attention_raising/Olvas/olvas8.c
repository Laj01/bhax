#include <stdio.h>

int f (int *a)
{
	return ++*a;
}

int main()
{
	int a = 0;
	printf("%d %d\n", f(&a), a);
		
	return 0;
}