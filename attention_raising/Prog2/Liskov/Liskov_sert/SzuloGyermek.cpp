#include <iostream>

using namespace std;


	class Szulo
	{
		public:
	 	void print1()
	 	{
			cout << "Én vagyok a szülő." << endl;
		}
	};

	class Gyerek : public Szulo
	{
		public:
		 void print2()
		{
			cout << "Én vagyok a gyermek." << endl;
		}
	};

int main()
{
	
	Szulo* sz = new Gyerek();

	sz->print1();
	sz->print2();

}