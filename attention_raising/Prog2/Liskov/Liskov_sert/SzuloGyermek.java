public class SzuloGyermek
{

	public static void main(String[] args)
	{
		Szulo sz = new Gyerek();
		sz.print1();
		sz.print2();
	}
}

class Szulo
{
	public void print1()
	{
		System.out.println("Én vagyok a szülő.");
	}
}

class Gyerek extends Szulo
{
	public void print2()
	{
		System.out.println("Én vagyok a gyermek.");
	}
}