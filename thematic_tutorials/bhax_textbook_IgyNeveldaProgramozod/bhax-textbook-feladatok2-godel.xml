<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Gödel!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>
            Gengszterek
        </title>
        <para>
            Gengszterek rendezése lambdával a Robotautó Világbajnokságban
            <link xlink:href="https://youtu.be/DL6iQwPx1Yw">https://youtu.be/DL6iQwPx1Yw</link>
            (8:05-től)
        </para>
        <para>
            Megoldás forrása:
            <link xlink:href="https://en.cppreference.com/w/cpp/language/lambda">https://en.cppreference.com/w/cpp/language/lambda</link>
        </para>
        <para>
            <link xlink:href="https://github.com/nbatfai/robocar-emulator">https://github.com/nbatfai/robocar-emulator</link>
        </para>
        <para>
            A <link xlink:href="https://en.cppreference.com/w/cpp/language/lambda">Lambda kifejezések</link> 
            a C++11-es verzióban jelentek meg először. Segítségükkel függvényeket hozhatunk létre, 
            anélkül, hogy deklarálnunk és definiálnunk kéne őket. Furcsán hangzik, kicsit 
            olyan, mintha egy "egyszerhasználatos-eldobható" függvényt készítenénk. 
            Ami külalakjában inkább hasonlít egy egyszerű változóra, mint egy konkrét függvényre. 
            Általános alakja:
        </para>
        <screen>
<![CDATA[[](paraméterek){lambda törzse / függvénytörzs}
]]></screen>
        <para>
            Az OOCWC-ben található Lambda:
            <programlisting language="c++">
<![CDATA[
std::sort ( gangsters.begin(), gangsters.end(), [this, cop] ( Gangster x, Gangster y )
  {
    return dst ( cop, x.to ) < dst ( cop, y.to );
  } );]]></programlisting>
        </para>
        <para>
            A <function>sort</function> függvény fogja rendezni a tárolt objektumokat/értékeket 
            miután megadjuk neki az első és utolsó elemet. Majd megkapja a felhasználótól azt a 
            lambda kifejezést, ami alapján ő rendezni szeretne.
        </para>
        <para>
            Forrás 1:       
            <link xlink:href="../../../bhax/attention_raising/Prog2/Godel/Lambda/myshmclient.cpp">
                <filename>bhax/attention_raising/Prog2/Godel/Lambda/myshmclient.cpp</filename>
            </link>
        </para>
    </section>
    <section>
        <title>
            C++11 Custom Allocator
        </title>
        <para>
            <link xlink:href="https://prezi.com/jvvbytkwgsxj/high-level-programming-languages-2-c11-allocators/">https://prezi.com/jvvbytkwgsxj/high-level-programming-languages-2-c11-allocators/</link> 
            a CustomAlloc-os példa, lásd C forrást az UDPROG repóban!
        </para>
    </section>
    <section>
        <title>
            STL map érték szerinti rendezése
        </title>
        <para>
            Például: <link xlink:href="https://github.com/nbatfai/future/blob/master/cs/F9F2/fenykard.cpp#L180">https://github.com/nbatfai/future/blob/master/cs/F9F2/fenykard.cpp#L180</link>
        </para>
        <para>
            A használni kívánt kulcs-érték párokat a <prompt>map</prompt> adatszerkezetben 
            tároljuk, amivel az az egyetlen probléma, hogy ezt önmagában nem tudjuk 
            rendezni a <function>sort</function> függvénnyel. Hogy ezt mégis megtehessük, 
            a string-int párokat áthelyezzük egy vektorba a  <link xlink:href="https://en.cppreference.com/w/cpp/utility/pair">pair</link> 
            függvény segítségével. Így két heterogén elemet már egy egységként tudunk tárolni. 
            A <function>sort</function> függvény meghívásával már az így létrejött 
            egységeket fogjuk rendezni minden gond nélkül.
        </para>
        <programlisting language="c++">
<![CDATA[
std::vector<std::pair<std::string, int>> sort_map ( std::map <std::string, int> &rank )
{
        std::vector<std::pair<std::string, int>> ordered;

        for ( auto & i : rank ) {
                if ( i.second ) {
                        std::pair<std::string, int> p {i.first, i.second};
                        ordered.push_back ( p );
                }
        }

        std::sort (
                std::begin ( ordered ), std::end ( ordered ),
        [ = ] ( auto && p1, auto && p2 ) {
                return p1.second > p2.second;
        }
        );

        return ordered;
}]]></programlisting>
    </section>
    <section>
        <title>
            Alternatív Tabella rendezése
        </title>
        <para>
            Mutassuk be a <link xlink:href="https://progpater.blog.hu/2011/03/11/alternativ_tabella">https://progpater.blog.hu/2011/03/11/alternativ_tabella</link>
            programban a java.lang Interface Comparable&#60;T&#62; szerepét!
        </para>
        <para>
            Az <link xlink:href="https://docs.oracle.com/javase/8/docs/api/java/lang/Comparable.html">Oracle doksi</link> 
            szerint a Comparable osztály egy teljes rendezést hajt végre az őt tartalmazó 
            minden egyes osztály összes objektumán. Erre a fajta rendezésre egy osztály "természetes 
            rendezéseként", a compareTo metódusra pedig az osztály természetes rendezési metódusaként 
            szoktak hivatkozni. Azon objektumok listái vagy tömbjei, amik ezt az interfészt használják 
            automatikusan rendezhetők a <link xlink:href="https://docs.oracle.com/javase/8/docs/api/java/util/Collections.html#sort-java.util.List-">Collections.sort</link> 
            vagy a <link xlink:href="https://docs.oracle.com/javase/8/docs/api/java/util/Arrays.html#sort-java.lang.Object:A-">Arrays.sort</link> 
            metódusok felhasználásával.
        </para>
        <para>
            A java.lang Interface Comparable&#60;T&#62; megjelenése az említett programban:
        </para>
        <programlisting language="java">
<![CDATA[class Csapat implements Comparable<Csapat> {

  protected String nev;
  protected double ertek;

  public Csapat(String nev, double ertek) {
    this.nev = nev;
    this.ertek = ertek;
  }

  public int compareTo(Csapat csapat) {
    if (this.ertek < csapat.ertek) {
      return -1;
    } else if (this.ertek > csapat.ertek) {
      return 1;
    } else {
      return 0;
    }
  }
}]]></programlisting>
        <para>
            TLDR: Mivel szeretnénk a csapatokat tartalmazó, listává alakított tömböt rendezni 
            egy <prompt>sort</prompt> metódussal, ezért implementálnunk kell a Comparable interfészt.
        </para>
        <para>
            Forrás 1:       
            <link xlink:href="../../../bhax/attention_raising/Prog2/Godel/Tabella/AlternativTabella.java">
                <filename>bhax/attention_raising/Prog2/Godel/Tabella/AlternativTabella.java</filename>
            </link>
        </para>
    </section>
    <section>
        <title>
            Prolog családfa
        </title>
        <para>
            Ágyazd be a Prolog családfa programot C++ vagy Java programba! Lásd 
            <link xlink:href="https://gitlab.com/nbatfai/pasigraphy-rhapsody/blob/master/para/docs/para_prog_guide.pdf">para_prog_guide.pdf</link> !
        </para>
    </section>
    <section>
        <title>
            GIMP Scheme hack
        </title>
        <para>
            Ha az előző félévben nem dolgoztad fel a témát (például a mandalás vagy a króm szöveges
            dobozosat) akkor itt az alkalom!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/OKdAkI_c7Sc">https://youtu.be/OKdAkI_c7Sc</link>      
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/GIMP_Lisp/Chrome">https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/GIMP_Lisp/Chrome</link>               
        </para>
        <para>
            Megoldás forrása: <link xlink:href="http://penguinpetes.com/b2evo/index.php?p=351">http://penguinpetes.com/b2evo/index.php?p=351</link>
        </para> 
        <para>
            A króm effektus elkészíthető a <link xlink:href="http://penguinpetes.com/b2evo/index.php?p=351">http://penguinpetes.com/b2evo/index.php?p=351</link>
            oldalon található grafikusan abrázolt tutoriál segítségével lépésenként is. De ezzel analóg módon 
            megírható egy script-fu kiterjesztés is, melyet majd a GIMP-ból előhívva mindössze néhány kattintással megcsinálja 
            a kívánt króm effektet "helyettünk". 
        </para>
        <para>
            Létrehozzuk a lokális változókat a függvény elején. Az itt használt függvényhívások 
            egyszerűen visszakereshetők a GIMP beépített <prompt>Eljárásböngésző</prompt> ablakában.
            <figure>
                <title></title>
                <mediaobject>
                    <imageobject>
                        <imagedata fileref="img/eljarasbongeszo.png" scale="30" />
                    </imageobject>
                    <textobject>
                        <phrase>Eljárásböngésző</phrase>
                    </textobject>
                </mediaobject>
            </figure>
            Új képet készítünk a megadott paraméterekkel (szélesség/magasság/típus). (Minden függvény visszaad egy 
            listát. Majd a visszatérési értéket betesszük a kívánt változóba.) 
            <programlisting language = "lisp"><![CDATA[(define (script-fu-bhax-chrome text font fontsize width height color gradient)
(let*
    (
        (image (car (gimp-image-new width height 0)))
        (layer (car (gimp-layer-new image width height RGB-IMAGE "bg" 100 LAYER-MODE-NORMAL-LEGACY)))
        (textfs)
        (text-width (car (text-wh text font fontsize)))
        (text-height (elem 2 (text-wh text font fontsize)))
        (layer2)        
    )]]></programlisting>
        </para>
        <para>
            1. lépés: Az előteret beállítjuk feketének. (az aposztof jel a '(0 0 0) kifejezés előőtt azért szükséges, 
            hogy a lisp adatnak tekintse az elő nullát és ne függvénynévnek. Így nem kezdi el kiértékelni.)
            Az előtérszínnel kiszínezzük az egész vásznat, majd visszaállítjuk az előteret fehérre.
            Középre pozícionáljuk a szöveget, majd legvégül rétegeket összefésüljök, hogy csak egy legyen.
            <programlisting language = "lisp"><![CDATA[    (gimp-image-insert-layer image layer 0 0)
    (gimp-context-set-foreground '(0 0 0))
    (gimp-drawable-fill layer  FILL-FOREGROUND )
    (gimp-context-set-foreground '(255 255 255))
   
    (set! textfs (car (gimp-text-layer-new image text font fontsize PIXELS)))
    (gimp-image-insert-layer image textfs 0 0)   
    (gimp-layer-set-offsets textfs (- (/ width 2) (/ text-width 2)) (- (/ height 2) (/ text-height 2)))
   
    (set! layer (car(gimp-image-merge-down image textfs CLIP-TO-BOTTOM-LAYER)))]]></programlisting>
        </para>
        <para>
            2. lépés: Az ún. Gauss-szűrőt alkalmazzuk a képen, ezzel fogjuk elmostni a betűszéleket. 
            Mivel szeretnénk, hogy a szoftver magától dolgozzon ezért nem lesz interaktív és nem fog a felhasználótól 
            értéket kérni.
            <programlisting language = "lisp"><![CDATA[(plug-in-gauss-iir RUN-INTERACTIVE image layer 15 TRUE TRUE)]]></programlisting>
        </para>
        <para>
            3. lépés: Az előbb elmosott kép szélét kicsit élesítjük. Ezáltal a betűk pontosabban kivehetők lesznek. 
            Formájuk lekerekedik. A színintenzitás alsó és felső határait állítjuk be.
            <programlisting language = "lisp"><![CDATA[(gimp-drawable-levels layer HISTOGRAM-VALUE .11 .42 TRUE 1 0 1 TRUE)]]></programlisting>
        </para>
        <para>
            4. lépés: Ismét ráengedjük a Gauss-szűrőt, lágyítani szeretnénk a képen, de most nem olyan drasztikusan mint a 2. lépésben.
            <programlisting language = "lisp"><![CDATA[(plug-in-gauss-iir RUN-INTERACTIVE image layer 2 TRUE TRUE)]]></programlisting>
        </para>
        <para>
            5. lépés: Kiválasztjuk a fekete területet és invertáljuk a szelekciót. 
            
            <programlisting language = "lisp"><![CDATA[    (gimp-image-select-color image CHANNEL-OP-REPLACE layer '(0 0 0))
    (gimp-selection-invert image)]]></programlisting>
        </para>
        <para>
            6. lépés: Létrehozunk egy új, transzparens réteget (az átlátszósága 100 lesz). Hzzáadjuk az új layert a képhez.
            <programlisting language = "lisp"><![CDATA[    (set! layer2 (car (gimp-layer-new image width height RGB-IMAGE "2" 100 LAYER-MODE-NORMAL-LEGACY)))
    (gimp-image-insert-layer image layer2 0 0)]]></programlisting>
        </para>
        <para>
            7. lépés: Az átlátszó réteget egy olyan gradienssel töltjük fel, ami a sötét-szürkéből a világos szürkébe tart.
            <programlisting language = "lisp"><![CDATA[	(gimp-context-set-gradient gradient) 
	(gimp-edit-blend layer2 BLEND-CUSTOM LAYER-MODE-NORMAL-LEGACY GRADIENT-LINEAR 100 0 REPEAT-NONE 
        FALSE TRUE 5 .1 TRUE width (/ height 3) width  (- height (/ height 3)))]]></programlisting>
        </para>
        <para>
            8. lépés: Bump map-eljük a 2. réteget (magyarul ez a csodálatos "Bucka leképezés" nevet kapta).
            Szabályozzuk a kiemelés és az eltolás mértékét is.
            <programlisting language = "lisp"><![CDATA[    (plug-in-bump-map RUN-NONINTERACTIVE image layer2 layer 120 25 7 5 5 0 0 TRUE FALSE 2)]]></programlisting>
        </para>
        <para>
            9. lépés: Legvégül a színgörbék beállítása. Itt kapja meg a szöveg a kívánt "fém effektet".
            Itt a bádogfazék tompa szürkéjétől egészen a T-1000-es higany textúrájáig mindent be tudunk állítani.
            Bár érdemes az arany középutat választani. A görbék kontrollpontjait x-y koordinátapárjukkal adjuk meg.
            <programlisting language = "lisp"><![CDATA[    (gimp-curves-spline layer2 HISTOGRAM-VALUE 8 (color-curve))
      
    (gimp-display-new image)
    (gimp-image-clean-all image)]]></programlisting>
        </para>
        <figure>
            <title></title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/chrome.png" scale="90" />
                </imageobject>
                <textobject>
                    <phrase>Kezdetleges króm effekt (a választott szó a Placeholder, nem maga a kép)</phrase>
                </textobject>
            </mediaobject>
        </figure>  
        <para>
            Forrás 1:
            <link xlink:href="../../../bhax/attention_raising/GIMP/GIMP_chrome.scm">
                <filename>bhax/attention_raising/GIMP/GIMP_chrome.scm</filename>
            </link>
        </para>
    </section>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
</chapter>                
